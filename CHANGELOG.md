# Changelog
## 2021-01-15
### Added
-  scroll to infobar when viewing comment permalink (#78) 
### Fixed
- Fix sidebar overflow on mobile (#109)
## 2021-01-12
### Added
- Added r/popular to list of subreddits
## 2021-01-10
### Added
- Edit date for comments
### Fixed
- Position of subscribe button in mobile
- Inconsistency of Link colours
## 2021-01-09
### Added
- User info on top of entries
- r/all even when users have subscriptions
### Fixed
- Previous/Next links on page.
## 2021-01-08
### Added
- Subscribe to subreddits and manage subscriptions from preferences page.
### Fixed
- Fixed subreddit view when there are no subscriptions.








